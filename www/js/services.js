angular.module('starter.services', ['ngSanitize', 'ui.select'])

/**
 * A simple example service that returns some data.
 */
.filter('propsFilter', function() {
  return function(items, props) {
    var out = [];

    if (angular.isArray(items)) {
      items.forEach(function(item) {
        var itemMatches = false;

        var keys = Object.keys(props);
        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();
          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    } else {
      out = items;
    }

    return out;
  };
})

.config(function($httpProvider) {
  $httpProvider.interceptors.push(function($rootScope) {
    return {
      request: function(config) {
        $rootScope.$broadcast('loading:show')
        return config
      },
      response: function(response) {
        $rootScope.$broadcast('loading:hide')
        return response
      }
    }
  })
})
.run(function ($rootScope, $ionicLoading, $window, $location) {
    // <summary>Configure the loading icon for http call.</summary>
    // <param name="$rootScope">Injects the $rootScope provider into the function</param>
    // <param name="$ionicLoading">Define the template of the loading icon.</param>
    // <param name="templateUrl">Defines the URL of the partial view.</param>
  $rootScope.$on('loading:show', function() {
      $ionicLoading.show({ template: '<span style="font-size:32px"><i class="icon ion-loading-c"></i></span>' })
  })

  $rootScope.$on('loading:hide', function() {
    $ionicLoading.hide()
  })
  //Set the default timeout for http calls
  $rootScope.timeout=15000;
    //Set the Default URL of Get
  $rootScope.apiUrlHomeGet = "http://lion.27coupons.com/v1.0/get/";
    // <summary>Checks for an authorized user when the URL changes and redirects to the Login screen if unauthorized</summary>
    // <returns></returns>
    // <remarks></remarks>
  if ($window.localStorage["userInfo"]!="{}" && $window.localStorage["userInfo"]!="undefined" &&$window.localStorage["userInfo"]!=null ) {
    var userInfo = JSON.parse($window.localStorage["userInfo"]);
    $rootScope.apiKeyOfUser=userInfo.apiKey;
    if($location.url()=="/app/splash")
    {
      $location.path("app/home");
    }
    }else{
      $location.path("/app/splash");
    }

})
/*
    <summary>All the servises makes either a get or a post request. They can be reused at a later stage</summary>
    <param name="$scope">Its a variable that is available to both the controller and view</param>
    <returns></returns>
    <remarks></remarks>
   */
.service("homePageService", ["$http","$rootScope","$window","$ionicModal","$ionicNavBarDelegate", function ($http,$rootScope,$window,$ionicModal,$ionicNavBarDelegate) {
      
    //fetches the home pages.
    this.gettabs = function ($scope,$ionicLoading) {
        if ($rootScope.apiKeyOfUser == null || $rootScope.apiKeyOfUser == "") {
            var userInfo = JSON.parse($window.localStorage["userInfo"]);
            $rootScope.apiKeyOfUser = userInfo.apiKey;
        }
          var Apikey = $rootScope.apiKeyOfUser;
              
                $http({
                method: 'POST',
                url: 'http://lion.27coupons.com/v1.0/post/coupons/home-page/',
                data: $.param({ 
                        key:Apikey,
                        //id:cpnId
                    }),
                timeout : $rootScope.timeout, 
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function (result) {

                    $scope.tabs=result.response;
                    if(result.status == 304)
                    {
                    $scope.message=result.response;

                    $ionicModal.fromTemplateUrl('templates/save.html', {
                      scope: $scope
                    }).then(function(modal) {
                      $scope.modal = modal;
                      $scope.modal.show();
                    });

                    // Triggered in the login modal to close it
                    $scope.closeLogin = function() {
                      $scope.modal.hide();
                      $('.outer-div').removeClass('blur-in');   
                    }; 
                  }
                    if(result.response.push_notifications.new != 0 )
                    {
                      $scope.not_hide = 1;
                    }
                }).error(function(data, status, header, config){
                    //alert(config.timeout);
                    $scope.error = true;

                  $scope.message="Please try again later";
                });

    }
}])
.service("singleCouponService", ["$http", "$rootScope", "$ionicModal", function ($http, $rootScope, $ionicModal) {
    //fetches the details of a single coupon
    this.details = function ($scope, $id) {
        $http.get($rootScope.apiUrlHomeGet+"coupons/list-single/?key="+$rootScope.apiKeyOfUser,{params: {id: $id}},{timeout : $rootScope.timeout})
                .success(function (result) {
                  
                    $scope.single_coupon=result.response.coupon;
                }).error(function(data, status, header, config){
                  //alert(config.timeout);
                  $scope.message="Please try again later";

                    $ionicModal.fromTemplateUrl('templates/save.html', {
                      scope: $scope
                    }).then(function(modal) {
                      $scope.modal = modal;
                      $scope.modal.show();
                    });

                    // Triggered in the login modal to close it
                    $scope.closeLogin = function() {
                      $scope.modal.hide();
                      $('.outer-div').removeClass('blur-in');   
                    };
                });

    }
}])

.service("notificationService", ["$http","$rootScope","$ionicModal", function ($http,$rootScope,$ionicModal) {
    //fetches the details of single notification
    this.show = function ($scope, $id) {
        $http.get($rootScope.apiUrlHomeGet+"notifications/list-all/?key="+$rootScope.apiKeyOfUser,{params: {id: $id}},{timeout : $rootScope.timeout})
                .success(function (result) {
                    $scope.notification=result.response;
                     //$scope.tabs=result.response;
                    if(result.status == 304)
                    {
                    $scope.message=result.response;

                    $ionicModal.fromTemplateUrl('templates/save.html', {
                      scope: $scope
                    }).then(function(modal) {
                      $scope.modal = modal;
                      $scope.modal.show();
                    });

                    // Triggered in the login modal to close it
                    $scope.closeLogin = function() {
                      $scope.modal.hide();
                      $('.outer-div').removeClass('blur-in');   
                    }; 
                  }
                }).error(function(data, status, header, config){
                  //alert(config.timeout);
                  $scope.message="Please try again later";

                    $ionicModal.fromTemplateUrl('templates/save.html', {
                      scope: $scope
                    }).then(function(modal) {
                      $scope.modal = modal;
                      $scope.modal.show();
                    });

                    // Triggered in the login modal to close it
                    $scope.closeLogin = function() {
                      $scope.modal.hide();
                      $('.outer-div').removeClass('blur-in');   
                    };
                });

    }
    //deletes a single notification
    this.delete = function ($scope) {
      $http.get($rootScope.apiUrlHomeGet+"notifications/remove-notifications/?key="+$rootScope.apiKeyOfUser,{timeout : $rootScope.timeout})
                .success(function (result) {
                }).error(function(data, status, header, config){
                  //alert(config.timeout);
                  $scope.message="Please try again later";

                    $ionicModal.fromTemplateUrl('templates/save.html', {
                      scope: $scope
                    }).then(function(modal) {
                      $scope.modal = modal;
                      $scope.modal.show();
                    });

                    // Triggered in the login modal to close it
                    $scope.closeLogin = function() {
                      $scope.modal.hide();
                      $('.outer-div').removeClass('blur-in');   
                    };
                });
    }
}])
.service("notificationDetailService", ["$http", "$rootScope", "$location", "$ionicModal", function ($http, $rootScope, $location, $ionicModal) {
    //selects a single notification
    this.select = function ($scope) {
        $http.get("http://lion.27coupons.com/"+$scope.api_node+"?key="+$rootScope.apiKeyOfUser,{params: {id: $scope.id}},{timeout : $rootScope.timeout})
                .success(function (result) {
                    if(($scope.category == "coupons") && ($scope.fourcharacter == "list-single"))
                    {
                       $scope.single_coupon=result.response;
                       $location.path("/app/couponsingle/"+$scope.id);
                    }
                    else if(($scope.category == "notifications") && ($scope.fourcharacter == "list-html"))
                    {
                      
                          $scope.message=result.response.content;

                          $ionicModal.fromTemplateUrl('templates/notification_html.html', {
                            scope: $scope
                          }).then(function(modal) {
                            $scope.modal = modal;
                            $scope.modal.show();
                          });

                          // Triggered in the login modal to close it
                          $scope.closeLogin = function() {
                            $scope.modal.hide();
                            $('.outer-div').removeClass('blur-in');   
                          }; 
                        
                    }
                    else if(($scope.category == "alerts") && ($scope.fourcharacter == "list-single"))
                    {
                        $scope.listsingle=result.response;
                       $location.path("/app/alertsingle/"+$scope.id);
                    }
                     else if(($scope.category == "stores") && ($scope.fourcharacter == "list-single"))
                    {
                        $scope.single_stores=result.response;
                       $location.path("/app/storelistsingle/"+$scope.id);
                    }
                     else if(($scope.category == "categories") && ($scope.fourcharacter == "list-single"))
                    {
                        $scope.categories_listsingle=result.response;
                       $location.path("/app/categoriessingle/"+$scope.id);
                    }
                     else if(($scope.category == "banks") && ($scope.fourcharacter == "list-single"))
                    {
                        $scope.banksingledetails=result.response;
                       $location.path("/app/banksingle/"+$scope.id);
                    }
                }).error(function(data, status, header, config){
                  //alert(config.timeout);
                  $scope.message="Please try again later";

                    $ionicModal.fromTemplateUrl('templates/save.html', {
                      scope: $scope
                    }).then(function(modal) {
                      $scope.modal = modal;
                      $scope.modal.show();
                    });

                    // Triggered in the login modal to close it
                    $scope.closeLogin = function() {
                      $scope.modal.hide();
                      $('.outer-div').removeClass('blur-in');   
                    };
                });

    }
}])
.service("pointsService", ["$http", "$rootScope", "$ionicModal", function ($http, $rootScope, $ionicModal) {
    //fetches the points
    this.show = function ($scope) {
        $http.get($rootScope.apiUrlHomeGet+"misc/points-summary/?key="+$rootScope.apiKeyOfUser,{timeout : $rootScope.timeout})
                .success(function (result) {
                    $scope.points=result.response.data;
                    if(result.response.data.total_points == null)
                    {
                      $scope.points.total_points = 0;
                    }
                }).error(function(data, status, header, config){
                  //alert(config.timeout);
                  $scope.message="Please try again later";

                    $ionicModal.fromTemplateUrl('templates/save.html', {
                      scope: $scope
                    }).then(function(modal) {
                      $scope.modal = modal;
                      $scope.modal.show();
                    });
                    // Triggered in the login modal to close it
                    $scope.closeLogin = function() {
                      $scope.modal.hide();
                      $('.outer-div').removeClass('blur-in');   
                    };
                });

    }
}])
.service("favouriteStoresService", ["$http", "$rootScope", "$ionicModal", function ($http, $rootScope, $ionicModal) {

    //fetches the favourite stores
    this.favstores = function ($scope) {
        $http.get($rootScope.apiUrlHomeGet+"stores/favorites/?key="+$rootScope.apiKeyOfUser,{timeout : $rootScope.timeout})
                .success(function (result) {
                    
                    if(result.status == 304)
                    {
                  
                        $scope.stores.count = 0;
                    } else {
                        $scope.favourite_stores = result.response.data.stores;
                        angular.forEach($scope.favourite_stores, function (stor) {
                            $scope.stores.push(stor);
                        });
                        $scope.paginationData = result.response.data.pagination;
                       }


                  

                }).error(function(data, status, header, config){
                  $scope.message="Please try again later";

                    $ionicModal.fromTemplateUrl('templates/save.html', {
                      scope: $scope
                    }).then(function(modal) {
                      $scope.modal = modal;
                      $scope.modal.show();
                    });

                    $scope.closeLogin = function() {
                      $scope.modal.hide();
                      $('.outer-div').removeClass('blur-in');   
                    };
                });

    }
    //loads more stores
    this.loadMoreStores = function ($scope) {
       // console.log($scope.page);
        $http.get($rootScope.apiUrlHomeGet + "stores/favorites/?key=" + $rootScope.apiKeyOfUser + "&page=" + $scope.page, { timeout: $rootScope.timeout })
               .success(function (result) {
                   $scope.favourite_stores = result.response.data.stores;
                   angular.forEach($scope.favourite_stores, function (stor) {
                       $scope.stores.push(stor);
                   });
                   $scope.paginationData = result.response.data.pagination;

               }).finally(function () {
                   $scope.$broadcast('scroll.infiniteScrollComplete');
               });
    }
}])
.service("singleStoreService", ["$http", "$rootScope", "$ionicModal", function ($http, $rootScope, $ionicModal) {

    //fetches the details of a single store
    this.details = function ($scope, $id) {
        $http.get($rootScope.apiUrlHomeGet+"stores/list-single/?key="+$rootScope.apiKeyOfUser,{params: {id: $id}},{timeout : $rootScope.timeout})
                .success(function (result) {
                    $scope.single_stores = result.response.data;
                    $scope.tabs = result.response.data.tabs;
                }).error(function(data, status, header, config){
                    $scope.error = true;
                  $scope.message="Please try again later";

                    $ionicModal.fromTemplateUrl('templates/save.html', {
                      scope: $scope
                    }).then(function(modal) {
                      $scope.modal = modal;
                      $scope.modal.show();
                    });

                    // Triggered in the login modal to close it
                    $scope.closeLogin = function() {
                      $scope.modal.hide();
                      $('.outer-div').removeClass('blur-in');   
                    };
                });

    }
}])
.service("featuredService", ["$http","$rootScope","$ionicModal", function ($http,$rootScope,$ionicModal) {
    //fetches the featured stores.
    this.feature = function ($scope) {
        $http.get($rootScope.apiUrlHomeGet+"stores/featured/?key="+$rootScope.apiKeyOfUser,{timeout : $rootScope.timeout})
                .success(function (result) {
                    $scope.featured=result.response.data;
                    if (result.response.push_notifications.new == 0) {
                     $scope.notification=result.response.push_notifications;
                     $scope.noti_hide = true;
                    };
                }).error(function(data, status, header, config){
                  //alert(config.timeout);
                  $scope.message="Please try again later";

                    $ionicModal.fromTemplateUrl('templates/save.html', {
                      scope: $scope
                    }).then(function(modal) {
                      $scope.modal = modal;
                      $scope.modal.show();
                    });

                    // Triggered in the login modal to close it
                    $scope.closeLogin = function() {
                      $scope.modal.hide();
                      $('.outer-div').removeClass('blur-in');   
                    };
                });

    }
}])

.service("searchService", ["$http", "$rootScope", "$ionicModal", function ($http, $rootScope, $ionicModal) {
    //serches the stores on the server
    this.store = function ($scope) {
        $http.get($rootScope.apiUrlHomeGet + "stores/search/?key=" + $rootScope.apiKeyOfUser, { params: { name: $scope.search_text } })
                .success(function (result) {
                    $scope.search = result.response.data;
                    $scope.paginationData = result.response.data.pagination;
                    $scope.searchStores = result.response.data.stores;
                });

    };
    //pagination handler
    this.searchMore = function ($scope) {
        $http.get($rootScope.apiUrlHomeGet + "stores/search/?key=" + $rootScope.apiKeyOfUser, { params: { name: $scope.search_text,page:$scope.paginationData.next_page } })
               .success(function (result) {
                   $scope.search = result.response.data;
                   $scope.paginationData = result.response.data.pagination;
                   
                   angular.forEach(result.response.data.stores, function (store) {
                       $scope.searchStores.push(store);
                   });
               });
    };
}])
 .service("storess", ["$http", "$rootScope", "$ionicModal", function ($http, $rootScope, $ionicModal) {
        this.stor = function ($scope) {
            $http.get($rootScope.apiUrlHomeGet + "alerts/stores/?key=" + $rootScope.apiKeyOfUser, { params: { name: $scope.search } })
                    .success(function (result) {
                        if (result.status == 304) {
                            $scope.message = result.response;

                            $ionicModal.fromTemplateUrl('templates/save.html', {
                                scope: $scope
                            }).then(function (modal) {
                                $scope.modal = modal;
                                $scope.modal.show();
                            });

                            // Triggered in the login modal to close it
                            $scope.closeLogin = function () {
                                $scope.modal.hide();
                                $('.outer-div').removeClass('blur-in');
                            };
                        }
                        $scope.store = result.response.stores;
                       // console.log(result);
                    });

        }
    }])

.service("couponService", ["$http","$rootScope","$ionicModal", function ($http,$rootScope,$ionicModal) {
    //fetches the saved coupons
    this.saved = function ($scope) {
        $http.get($rootScope.apiUrlHomeGet + "coupons/list-saved-coupons/?key=" + $rootScope.apiKeyOfUser, { timeout: $rootScope.timeout })
                .success(function (result) {
                    //$scope.saved = result.response.data;
                    console.log(result);
                    $scope.paginationData = result.response.data.pagination;
                    $scope.tabs = result.response.data.tabs;
                    $scope.totalRecords = result.response.data.total_records;

                    if (result.response.push_notifications.new == 0) {
                        $scope.notification = result.response.push_notifications;
                        $scope.noti_hide = true;
                    };
                }).error(function (data, status, header, config) {
                    //alert(config.timeout);
                    $scope.error = true;
                    $scope.message = "Please try again later";

                    $ionicModal.fromTemplateUrl('templates/save.html', {
                        scope: $scope
                    }).then(function (modal) {
                        $scope.modal = modal;
                        $scope.modal.show();
                    });

                    // Triggered in the login modal to close it
                    $scope.closeLogin = function () {
                        $scope.modal.hide();
                        $('.outer-div').removeClass('blur-in');
                    };
                });

    };
    //loads more coupons
    this.loadMoreResults = function ($scope) {
        $http.get($rootScope.apiUrlHomeGet + "coupons/list-saved-coupons/?key=" + $rootScope.apiKeyOfUser+"&page="+$scope.paginationData.next_page, { timeout: $rootScope.timeout })
            .success(function (result) {
                $scope.paginationData = result.response.data.pagination;
                $scope.newTabs = result.response.data.tabs;
                $scope.totalRecords = result.response.data.total_records;
                angular.forEach($scope.tabs, function (tab) {
                    angular.forEach($scope.newTabs, function (newTab) {
                        if (tab.name == newTab.name) {
                            angular.forEach(newTab.coupons, function (coupon) {
                                tab.coupons.push(coupon);
                            });
                        }

                    });
                   // console.log(tab.name);
                    //console.log(tab.coupons);
                });

            });
    };
}])
.service("categoriesListAllService", ["$http","$rootScope","$ionicModal", function ($http,$rootScope,$ionicModal) {
    //fetches the category list
    this.categorylist = function ($scope) {
        $http.get($rootScope.apiUrlHomeGet+"categories/list-all/?key="+$rootScope.apiKeyOfUser,{timeout : $rootScope.timeout})
                .success(function (result) {
                    $scope.categories_listall=result.response.data;
                    if (result.response.push_notifications.new == 0) {
                     $scope.notification=result.response.push_notifications;
                     $scope.noti_hide = true;
                    };
                }).error(function(data, status, header, config){
                  //alert(config.timeout);
                  $scope.message="Please try again later";

                    $ionicModal.fromTemplateUrl('templates/save.html', {
                      scope: $scope
                    }).then(function(modal) {
                      $scope.modal = modal;
                      $scope.modal.show();
                    });

                    // Triggered in the login modal to close it
                    $scope.closeLogin = function() {
                      $scope.modal.hide();
                      $('.outer-div').removeClass('blur-in');   
                    };
                });

    }
}])
.service("categoriesListsingleService", ["$http","$rootScope","$ionicModal", function ($http,$rootScope,$ionicModal) {
    //fetches the particular category
    this.categorydetails = function ($scope, $id) {
        $http.get($rootScope.apiUrlHomeGet+"categories/list-single/?key="+$rootScope.apiKeyOfUser,{params: {id: $id}},{timeout : $rootScope.timeout})
                .success(function (result) {
                    //$scope.categories_listsingle = result.response.data;

                    $scope.paginationData = result.response.data.pagination;
                    $scope.tabs = result.response.data.tabs;
                    $scope.category = result.response.data.category;
                    $scope.totalRecords = result.response.data.total_records;

                    if(result.status == 304)
                    {
                    $scope.message=result.response;

                    $ionicModal.fromTemplateUrl('templates/save.html', {
                      scope: $scope
                    }).then(function(modal) {
                      $scope.modal = modal;
                      $scope.modal.show();
                    });

                    // Triggered in the login modal to close it
                    $scope.closeLogin = function() {
                      $scope.modal.hide();
                      $('.outer-div').removeClass('blur-in');   
                    }; 
                  }
                }).error(function(data, status, header, config){
                  //alert(config.timeout);
                  $scope.message="Please try again later";

                    $ionicModal.fromTemplateUrl('templates/save.html', {
                      scope: $scope
                    }).then(function(modal) {
                      $scope.modal = modal;
                      $scope.modal.show();
                    });

                    // Triggered in the login modal to close it
                    $scope.closeLogin = function() {
                      $scope.modal.hide();
                      $('.outer-div').removeClass('blur-in');   
                    };
                });

    }
    //handles load more
    this.loadMoreCategories = function ($scope) {
        //alert("hi");
        $http.get($rootScope.apiUrlHomeGet + "categories/list-single/?key=" + $rootScope.apiKeyOfUser, { params: { id: $scope.category.id,page:$scope.paginationData.next_page } }, { timeout: $rootScope.timeout })
                .success(function (result) {
                    //$scope.categories_listsingle = result.response.data;
                    $scope.paginationData = result.response.data.pagination;
                    $scope.newTabs = result.response.data.tabs;
                    $scope.category = result.response.data.category;
                    $scope.totalRecords = result.response.data.total_records;

                    angular.forEach($scope.tabs, function (tab) {
                        angular.forEach($scope.newTabs, function (newTab) {
                            if (tab.name == newTab.name) {
                                angular.forEach(newTab.coupons, function (coupon) {
                                    tab.coupons.push(coupon);
                                });
                            }

                        });
                        
                    });
                });
    }
}])
.service("bankService", ["$http","$rootScope","$ionicModal", function ($http,$rootScope,$ionicModal) {
    //fetches the banks
    this.banklist = function ($scope) {
        $http.get($rootScope.apiUrlHomeGet+"banks/list-all/?key="+$rootScope.apiKeyOfUser,{timeout : $rootScope.timeout})
                .success(function (result) {

                    $scope.paginationData = result.response.data.pagination;
                    $scope.bankList = result.response.data.banks;
                    angular.forEach($scope.bankList, function (bank) {
                        $scope.banks.push(bank);
                    });
                    
                    if (result.response.push_notifications.new == 0) {
                     $scope.notification=result.response.push_notifications;
                     $scope.noti_hide = true;
                    };
                }).error(function(data, status, header, config){
                  //alert(config.timeout);
                  $scope.message="Please try again later";

                    $ionicModal.fromTemplateUrl('templates/save.html', {
                      scope: $scope
                    }).then(function(modal) {
                      $scope.modal = modal;
                      $scope.modal.show();
                    });

                    // Triggered in the login modal to close it
                    $scope.closeLogin = function() {
                      $scope.modal.hide();
                      $('.outer-div').removeClass('blur-in');   
                    };
                });
                return 1;
                    
    }
    //handles load more
    this.loadMoreBanks = function ($scope) {
        $http.get($rootScope.apiUrlHomeGet + "banks/list-all/?key=" + $rootScope.apiKeyOfUser + "&page=" + $scope.page, { timeout: $rootScope.timeout })
               .success(function (result) {
                   $scope.paginationData = result.response.data.pagination;
                   $scope.bankList = result.response.data.banks;
                   angular.forEach($scope.bankList, function (bank) {
                       $scope.banks.push(bank);
                   });
               }).finally(function () {
                   $scope.$broadcast('scroll.infiniteScrollComplete');
               });
    }
}])
.service("bankDetailsService", ["$http", "$rootScope", "$ionicModal", function ($http, $rootScope, $ionicModal) {
    //fetches a particular branch
    this.banksingledetails = function ($scope, $id) {
        $http.get($rootScope.apiUrlHomeGet + "banks/list-single/?key=" + $rootScope.apiKeyOfUser, { params: { id: $id } }, { timeout: $rootScope.timeout })
                .success(function (result) {
                    console.log(result.response);
                    $scope.banksingledetails = result.response.data;
                    $scope.paginationData = result.response.data.pagination;
                    $scope.bank = result.response.data.bank;
                    
                    $scope.couponList = result.response.data.coupons;
                    angular.forEach($scope.couponList, function (coupon) {
                        $scope.coupons.push(coupon);
                    });
                }).error(function (data, status, header, config) {
                    //alert(config.timeout);
                    $scope.message = "Please try again later";

                    $ionicModal.fromTemplateUrl('templates/save.html', {
                        scope: $scope
                    }).then(function (modal) {
                        $scope.modal = modal;
                        $scope.modal.show();
                    });

                    // Triggered in the login modal to close it
                    $scope.closeLogin = function () {
                        $scope.modal.hide();
                        $('.outer-div').removeClass('blur-in');
                    };
                });

    }
    //load more coupons for each branch
    this.loadMoreBanks = function ($scope) {
        $http.get($rootScope.apiUrlHomeGet + "banks/list-single/?key=" + $rootScope.apiKeyOfUser, { params: { id: $scope.bank.id,page:$scope.page } }, { timeout: $rootScope.timeout })
               .success(function (result) {
                   $scope.banksingledetails = result.response.data;
                   $scope.paginationData = result.response.data.pagination;
                   $scope.bank = result.response.data.bank;
                   $scope.couponList = result.response.data.coupons;
                   angular.forEach($scope.couponList, function (coupon) {
                       $scope.coupons.push(coupon);
                   });
               }).finally(function () {
                   $scope.$broadcast('scroll.infiniteScrollComplete');
               });
    }

    
}])
.service("alertService", ["$http","$rootScope","$ionicModal", function ($http,$rootScope,$ionicModal) {
    //list  a all alerts
    this.listsingle = function ($scope) {
        $http.get($rootScope.apiUrlHomeGet + "alerts/list-all/?key=" + $rootScope.apiKeyOfUser, { timeout: $rootScope.timeout })
                .success(function (result) {
                    $scope.listsingle = result.response.data;
                    if (result.status == 304) {
                        $scope.message = result.response;

                        $ionicModal.fromTemplateUrl('templates/save.html', {
                            scope: $scope
                        }).then(function (modal) {
                            $scope.modal = modal;
                            $scope.modal.show();
                            $('.favorite').removeClass('item')
                        });

                        // Triggered in the login modal to close it
                        $scope.closeLogin = function () {
                            $scope.modal.hide();
                        };
                    }
                }).error(function (data, status, header, config) {
                    $scope.error = true;
                });

    }
}])
.service("singleAlertService", ["$http","$rootScope","$ionicModal", function ($http,$rootScope,$ionicModal) {
    this.listsingle = function ($scope,$id) {
        $http.get($rootScope.apiUrlHomeGet+"alerts/list-single/?key="+$rootScope.apiKeyOfUser,{params: {id: $id}},{timeout : $rootScope.timeout})
                .success(function (result) {
                   // console.log(result.response);
                    //$scope.listsingle = result.response.data;
                    $scope.alert = result.response.data.alert;
                    $scope.totalRecords = result.response.data.total_records;
                    $scope.tabs = result.response.data.tabs;
                    $scope.paginationData = result.response.data.pagination;

                }).error(function(data, status, header, config){
                    //alert(config.timeout);
                    $scope.error = true;
                    $scope.message="Please try again later";

                    $ionicModal.fromTemplateUrl('templates/save.html', {
                      scope: $scope
                    }).then(function(modal) {
                      $scope.modal = modal;
                      $scope.modal.show();
                    });

                    // Triggered in the login modal to close it
                    $scope.closeLogin = function() {
                      $scope.modal.hide();
                      $('.outer-div').removeClass('blur-in');   
                    };
                    $scope.error = true;
                });

    }

    this.loadMoreAlerts = function ($scope) {
        $http.get($rootScope.apiUrlHomeGet + "alerts/list-single/?key=" + $rootScope.apiKeyOfUser, { params: { id: $scope.alert.id,page:$scope.paginationData.next_page } }, { timeout: $rootScope.timeout })
                .success(function (result) {
                    $scope.alert = result.response.data.alert;
                    $scope.totalRecords = result.response.data.total_records;
                    $scope.newTabs = result.response.data.tabs;
                    $scope.paginationData = result.response.data.pagination;
                    angular.forEach($scope.tabs, function (tab) {
                        angular.forEach($scope.newTabs, function (newTab) {
                            if (tab.name == newTab.name) {
                                angular.forEach(newTab.coupons, function (coupon) {
                                    tab.coupons.push(coupon);

                                });
                            }

                        });
                        //console.log(tab.name);
                        //console.log(tab.coupons);
                    });

                });
    };
}])
.service("createAlertService", ["$http","$rootScope","$ionicModal", function ($http,$rootScope,$ionicModal) {
    this.create = function ($scope) {
        $http.get($rootScope.apiUrlHomeGet+"alerts/create-alert/?key="+$rootScope.apiKeyOfUser,{timeout : $rootScope.timeout})
                .success(function (result) {
                    $scope.store = result.response.selected_stores;
                    $scope.categories = result.response.categories;
                    $scope.storefromServer = result.response.selected_stores;
                    angular.forEach($scope.storefromServer, function (store) {
                        $scope.form.stores.push(store);
                    });

                }).error(function(data, status, header, config){
                  //alert(config.timeout);
                  $scope.message="Please try again later";

                    $ionicModal.fromTemplateUrl('templates/save.html', {
                      scope: $scope
                    }).then(function(modal) {
                      $scope.modal = modal;
                      $scope.modal.show();
                    });

                    // Triggered in the login modal to close it
                    $scope.closeLogin = function() {
                      $scope.modal.hide();
                      $('.outer-div').removeClass('blur-in');   
                    };
                });

    }
}])
.service("deleteAlertService", ["$http", "$rootScope", "$ionicModal", "$location", "$cordovaToast", function ($http, $rootScope, $ionicModal, $location, $cordovaToast) {
        this.del = function ($scope) {

           var Apikey = $rootScope.apiKeyOfUser;
          var cpnId=$scope.alert_id;
              
                $http({
                method: 'POST',
                url: 'http://lion.27coupons.com/v1.0/post/alerts/delete-alert/',
                data: $.param({ 
                        key:Apikey,
                        id:cpnId
                    }),
                timeout : $rootScope.timeout,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(data){
                  $scope.message=data.response;
                  $cordovaToast.show($scope.message, "long", "center");
                  $location.path("/app/alert");
                }).error(function(data, status, header, config){
                  $scope.message="Please try again later";
                  $cordovaToast.show($scope.message, "long", "center");

                   
                });

    }
}])
.service("activateAlertService", ["$http", "$rootScope", "$ionicModal", "$cordovaToast", function ($http, $rootScope, $ionicModal, $cordovaToast) {
        this.activate = function ($scope) {

           var Apikey = $rootScope.apiKeyOfUser;
          var cpnId=$scope.alert_id;
              
                $http({
                method: 'POST',
                url: 'http://lion.27coupons.com/v1.0/post/alerts/activate-alert/',
                data: $.param({ 
                        key:Apikey,
                        id:cpnId
                    }),
                timeout : $rootScope.timeout,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(data){
                  $scope.message=data.response;
                  $cordovaToast.show($scope.message, "long", "center");

                }).error(function(data, status, header, config){
                  //alert(config.timeout);
                  $scope.message="Please try again later";
                  $cordovaToast.show($scope.message, "long", "center");

                });

    }
}])
.service("pauseAlertService", ["$http", "$rootScope", "$ionicModal", "$cordovaToast", function ($http, $rootScope, $ionicModal, $cordovaToast) {
        this.pause = function ($scope) {

           var Apikey = $rootScope.apiKeyOfUser;
          var cpnId=$scope.alert_id;
              
                $http({
                method: 'POST',
                url: 'http://lion.27coupons.com/v1.0/post/alerts/pause-alert/',
                data: $.param({ 
                        key:Apikey,
                        id:cpnId
                    }),
                timeout : $rootScope.timeout, 
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(data){
                  $scope.message=data.response;
                  $cordovaToast.show($scope.message, "long", "center");

                }).error(function(data, status, header, config){
                  //alert(config.timeout);
                  $scope.message="Please try again later";
                  $cordovaToast.show($scope.message, "long", "center");

                });

    }
}])
.service("saveAlertService", ["$http","$rootScope","$ionicModal","$location","$cordovaToast", function ($http,$rootScope,$ionicModal,$location,$cordovaToast) {
    this.create = function ($form,$scope) {
        var user_alert_store_ids="[";
        for(i=0;i<$form.store_id.length;i++){
          user_alert_store_ids+=$form.store_id[i]+',';
        }
        user_alert_store_ids= user_alert_store_ids.substring(0, user_alert_store_ids.length - 1);
        user_alert_store_ids+="]";

        var user_alert_cat_ids="[";
        for(i=0;i<$form.categories_id.length;i++){
          user_alert_cat_ids+=$form.categories_id[i]+',';
        }
        user_alert_cat_ids= user_alert_cat_ids.substring(0, user_alert_cat_ids.length - 1);
        user_alert_cat_ids+="]";

        var user_alert_schedule="[";
        for(i=0;i<$form.schedule_id.length;i++){
          user_alert_schedule+=$form.schedule_id[i]+',';
        }
        user_alert_schedule= user_alert_schedule.substring(0, user_alert_schedule.length - 1);
        user_alert_schedule+="]";

        var Apikey = $rootScope.apiKeyOfUser;
                $http({
                method: 'POST',
                url: 'http://lion.27coupons.com/v1.0/post/alerts/save-alert/',
                data: $.param({ 
                        key:Apikey,
                        user_alert_name:$form.username,
                        user_alert_store_ids:user_alert_store_ids,
                        user_alert_cat_ids:user_alert_cat_ids,
                        user_alert_schedule:user_alert_schedule,
                        user_alert_coupons_only:$form.coupon
                    }),
                timeout : $rootScope.timeout, 
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(data){

                     $scope.message=data.response;
                     $cordovaToast.show($scope.message, "long", "center");
                     $location.path("/app/alert");
                     $scope.$apply();

                }).error(function(data, status, header, config){
                  $scope.message="Please try again later";
                  $cordovaToast.show($scope.message, "long", "center");
                });

    }
}])
.service("authenticationSvc", function ($http, $q, $window, $location, $rootScope, $timeout, locationService) {
  var userInfo={};
  this.login=function (username,password,$scope) {
    var deferred = $q.defer();
    $http.get("http://lion.27coupons.com/v1.0/post/users/login/?user_email="+username+"&user_password="+password+"&user_network=27coupons",{timeout : $rootScope.timeout})
    .success(function(result) {
        if(result.status==200)
        {
            userInfo = {
                  apiKey: result.response.user_api_key,
                  userEmail:username     
            };
            $timeout(function() {
            $scope.closeLogin();
              }, 1000);
            $('.nav-bar').show();
            $window.localStorage["userInfo"] = JSON.stringify(userInfo);
            //locationService.sendLocation(result.response.user_api_key);
            $location.path("/app/home");
            $('.button-clear').remove();
            deferred.resolve(userInfo);

        }else{

            $('.error2').addClass('ng-hide');
            $('.error1').addClass('ng-hide');
            $('.error3').addClass('ng-hide');
            $('.sign-in-error').css('display','block');
            $scope.message = result.response;
        }
    }, function(error) {
      deferred.reject(error);
    });
 
    return deferred.promise;
  }
 

  //Register
  this.register= function (username,password,$scope) {
    var deferred = $q.defer();
    $http.get("http://lion.27coupons.com/v1.0/post/users/register/?user_email="+username+"&user_password="+password+"&user_network=27coupons",{timeout : $rootScope.timeout})
    .success(function(result) {
      if(result.status==200)
      {
        userInfo = {
                    apiKey: result.response.user_api_key,
                    userEmail:username     
                   };
      $window.localStorage["userInfo"] = JSON.stringify(userInfo);
      $scope.message = result.response;
      deferred.resolve(userInfo);
       $('.nav-bar').show();
       $scope.modal.hide();
       //locationService.sendLocation(result.response.user_api_key);
       $location.path("/app/home");
      }else{
        $('.sign-in-error').css('display','block');
         $scope.message = result.response;
      }
        
    }, function(error) {
      deferred.reject(error);
    });
 
    return deferred.promise;
  }


  //Reset Password
  this.resetPassword=function(userName,$scope){
    $http.get("http://lion.27coupons.com/v1.0/post/users/reset-password/?user_email="+userName,{timeout : $rootScope.timeout})
                .success(function (result) {
                    $scope.message = result.response;
                });

  }
  //Login with facebook
  this.loginWithFacebook=function (username) {
    var deferred = $q.defer();
    $http.get("http://lion.27coupons.com/v1.0/post/users/login/?user_email="+username+"&user_network=facebook",{timeout : $rootScope.timeout})
    .success(function(result) {
        if(result.status==200)
        {
            userInfo = {
                  apiKey: result.response.user_api_key,
                  userEmail:username     
            };
            $window.localStorage["userInfo"] = JSON.stringify(userInfo);
            //locationService.sendLocation(result.response.user_api_key);
            $location.path("/app/home");
            deferred.resolve(userInfo);
        }else{
        }
    }, function(error) {
      deferred.reject(error);
    });
 
    return deferred.promise;
  }
 
})
.service("favoriteService", ["$http", "$rootScope", "$ionicModal", "$cordovaToast", function ($http, $rootScope, $ionicModal, $cordovaToast) {
    this.save = function ($scope) {
    
          var Apikey = $rootScope.apiKeyOfUser;
          var cpnId=$scope.coupon_id;
              
                $http({
                method: 'POST',
                url: 'http://lion.27coupons.com/v1.0/post/coupons/save-coupon/',
                data: $.param({ 
                        key:Apikey,
                        id:cpnId
                    }),
                timeout : $rootScope.timeout,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(data){
                    $scope.single_coupon.fav = 1;
                    $scope.message=data.response;
                    $cordovaToast.show($scope.message , "long", "center");
                   
                }).error(function(data, status, header, config){
                  //alert(config.timeout);
                  $scope.message="Please try again later..";
                  $cordovaToast.show($scope.message, "long", "center");

                });
 

    }
     this.remove = function ($scope) {
         var Apikey = $rootScope.apiKeyOfUser;
          var cpnId=$scope.coupon_id;
              
                $http({
                method: 'POST',
                url: 'http://lion.27coupons.com/v1.0/post/coupons/remove-coupon/',
                data: $.param({ 
                        key:Apikey,
                        id:cpnId
                    }),
                timeout : $rootScope.timeout,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(data){
                    $scope.single_coupon.fav = 0;
                    $scope.message=data.response;
                    $cordovaToast.show($scope.message, "long", "center");

                   
                        }).error(function(data, status, header, config){
                  //alert(config.timeout);
                            $scope.message = "Please try again later..";
                            $cordovaToast.show($scope.message, "long", "center");

                });

    }
}])
.service("categorySaveService", ["$http", "$rootScope", "$ionicModal", "$cordovaToast", "$ionicLoading", function ($http, $rootScope, $ionicModal, $cordovaToast, $ionicLoading) {
    this.save = function ($scope) {

           var Apikey = $rootScope.apiKeyOfUser;
           var catId=$scope.category.id;
                $http({
                method: 'POST',
                url: 'http://lion.27coupons.com/v1.0/post/categories/save-category/',
                data: $.param({ 
                        key:Apikey,
                        id: catId
                    }),
                timeout :$rootScope.timeout,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(data){
                  $scope.category.fav = 1;
                  $scope.message = data.response;
                  $cordovaToast.show($scope.message, "long", "center");
                }).error(function(data, status, header, config){
                  //alert(config.timeout);
                    $scope.message = "Please try again later";
                 $ionicLoading.hide();
                  $cordovaToast.show($scope.message, "long", "center");
                });

    }
     this.remove = function ($scope) {
         var Apikey = $rootScope.apiKeyOfUser;
         var catId = $scope.category.id;

              
                $http({
                method: 'POST',
                url: 'http://lion.27coupons.com/v1.0/post/categories/remove-category/',
                data: $.param({ 
                        key:Apikey,
                        id: catId
                    }),
                timeout : $rootScope.timeout,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(data){
                    $scope.category.fav = 0;
                    $scope.message=data.response;
                    $cordovaToast.show($scope.message, "long", "center");
                }).error(function(data, status, header, config){
                  $scope.message="Please try again later";
                  $ionicLoading.hide();
                  $cordovaToast.show($scope.message, "long", "center");

                });

    }
}])
.service("categorySubscribeService", ["$http", "$rootScope", "$ionicModal", "$cordovaToast","$ionicLoading", function ($http, $rootScope, $ionicModal, $cordovaToast,$ionicLoading) {
    this.subscribe = function ($scope) {
          var Apikey = $rootScope.apiKeyOfUser;
          var catId=$scope.category.id;
              
                $http({
                method: 'POST',
                url: 'http://lion.27coupons.com/v1.0/post/categories/subscribe/',
                data: $.param({ 
                        key:Apikey,
                        id: catId
                    }),
                timeout : $rootScope.timeout,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(data){
                    $scope.category.alert = 1;
                    $scope.message = data.response;
                    $cordovaToast.show($scope.message, "long", "center");

                }).error(function(data, status, header, config){
                  //alert(config.timeout);
                  $scope.message="Please try again later";
                  $ionicLoading.hide();
                  $cordovaToast.show($scope.message, "long", "center");

                });

    }
      this.unsubscribe = function ($scope) {
         var Apikey = $rootScope.apiKeyOfUser;
         var catId = $scope.category.id;

              
                $http({
                method: 'POST',
                url: 'http://lion.27coupons.com/v1.0/post/categories/un-subscribe/',
                data: $.param({ 
                        key:Apikey,
                        id: catId
                    }),
                timeout : $rootScope.timeout,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(data){
                    $scope.category.alert = 0;
                    $scope.message=data.response;
                    $cordovaToast.show($scope.message, "long", "center");
 
                        }).error(function(data, status, header, config){
                  //alert(config.timeout);
                  $scope.message="Please try again later";
                  $ionicLoading.hide();
                  $cordovaToast.show($scope.message, "long", "center");

                });

    }

}])
.service("bankSaveService", ["$http", "$rootScope", "$ionicModal", "$cordovaToast","$ionicLoading" ,function ($http, $rootScope, $ionicModal, $cordovaToast,$ionicLoading) {
    this.save = function ($scope) {

            var Apikey = $rootScope.apiKeyOfUser;
          var cpnId=$scope.bank_id;
              
                $http({
                method: 'POST',
                url: 'http://lion.27coupons.com/v1.0/post/banks/save-bank/',
                data: $.param({ 
                        key:Apikey,
                        id:cpnId
                    }),
                timeout : $rootScope.timeout,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(data){
                    $scope.banksingledetails.bank.fav = 1;
                    $scope.message=data.response;
                
                }).error(function(data, status, header, config){
                  //alert(config.timeout);
                  $scope.message="Please try again later";
                  $ionicLoading.hide();
                  $cordovaToast.show($scope.message, "long", "center");

                });

    }
         this.remove = function ($scope) {
         var Apikey = $rootScope.apiKeyOfUser;
          var cpnId=$scope.bank_id;
              
                $http({
                method: 'POST',
                url: 'http://lion.27coupons.com/v1.0/post/banks/remove-bank/',
                data: $.param({ 
                        key:Apikey,
                        id:cpnId
                    }),
                timeout : $rootScope.timeout,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(data){
                    $scope.banksingledetails.bank.fav = 0;
                    $scope.message=data.response;
                    $cordovaToast.show($scope.message, "long", "center");

                     }).error(function(data, status, header, config){
                  //alert(config.timeout);
                  $scope.message="Please try again later..";
                  $ionicLoading.hide();
                  $cordovaToast.show($scope.message, "long", "center");

                });

    }

}])
.service("bankSubscribeService", ["$http", "$rootScope", "$ionicModal", "$cordovaToast","$ionicLoading", function ($http, $rootScope, $ionicModal, $cordovaToast,$ionicLoading) {
    this.subscribe = function ($scope) {

               var Apikey = $rootScope.apiKeyOfUser;
          var cpnId=$scope.bank_id;
              
                $http({
                method: 'POST',
                url: 'http://lion.27coupons.com/v1.0/post/banks/subscribe/',
                data: $.param({ 
                        key:Apikey,
                        id:cpnId
                    }),
                timeout : $rootScope.timeout,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(data){
                    $scope.message=data.response;
                    $scope.banksingledetails.bank.alert = 1;
                    $cordovaToast.show($scope.message, "long", "center");

                }).error(function(data, status, header, config){
                  //alert(config.timeout);
                  $scope.message="Please try again later";
                  $ionicLoading.hide();
                  $cordovaToast.show($scope.message, "long", "center");

                });

    }
       this.unsubscribe = function ($scope) {
         var Apikey = $rootScope.apiKeyOfUser;
          var cpnId=$scope.store_id;
              
                $http({
                method: 'POST',
                url: 'http://lion.27coupons.com/v1.0/post/banks/un-subscribe/',
                data: $.param({ 
                        key:Apikey,
                        id:cpnId
                    }),
                timeout : $rootScope.timeout,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(data){
                    $scope.message=data.response;
                    $scope.banksingledetails.bank.alert = 0;
                    
                        }).error(function(data, status, header, config){
                  $scope.message="Please try again later";
                  $ionicLoading.hide();
                  $cordovaToast.show($scope.message, "long", "center");

                });

    }
}])
.service("storeSaveSrevice", ["$http", "$rootScope", "$ionicModal", "$cordovaToast","$ionicLoading", function ($http, $rootScope, $ionicModal, $cordovaToast,$ionicLoading) {
    this.save = function ($scope) {

          var Apikey = $rootScope.apiKeyOfUser;
          var cpnId=$scope.store_id;
              
                $http({
                method: 'POST',
                url: 'http://lion.27coupons.com/v1.0/post/stores/save-store/',
                data: $.param({ 
                        key:Apikey,
                        id:cpnId
                    }),
                timeout : $rootScope.timeout,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(data){
                    $scope.single_stores.store.fav = 1;
                    $scope.message=data.response;
                    $cordovaToast.show($scope.message, "long", "center");

                }).error(function(data, status, header, config){
                  //alert(config.timeout);
                    $scope.message = "Please try again later..";
                    $ionicLoading.hide();
                    $cordovaToast.show($scope.message, "long", "center");

                });

    }

     this.remove = function ($scope) {
         var Apikey = $rootScope.apiKeyOfUser;
          var cpnId=$scope.store_id;
              
                $http({
                method: 'POST',
                url: 'http://lion.27coupons.com/v1.0/post/stores/remove-store/',
                data: $.param({ 
                        key:Apikey,
                        id:cpnId
                    }),
                timeout : $rootScope.timeout,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(data){
                    $scope.single_stores.store.fav = 0;
                    $scope.message=data.response;
                    $cordovaToast.show($scope.message, "long", "center");

                        }).error(function(data, status, header, config){
                  //alert(config.timeout);
                  $scope.message="Please try again later..";
                    $ionicLoading.hide();
                  $cordovaToast.show($scope.message, "long", "center");
                    
                });

    }
}])
.service("storeSubscribeService", ["$http", "$rootScope", "$ionicModal", "$cordovaToast","$ionicLoading", function ($http, $rootScope, $ionicModal, $cordovaToast,$ionicLoading) {
    this.subscribe = function ($scope) {

          var Apikey = $rootScope.apiKeyOfUser;
          var cpnId=$scope.store_id;
              
                $http({
                method: 'POST',
                url: 'http://lion.27coupons.com/v1.0/post/stores/subscribe/',
                data: $.param({ 
                        key:Apikey,
                        id:cpnId
                    }),
                timeout : $rootScope.timeout,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(data){
                    $scope.single_stores.store.alert = 1;
                    $scope.message=data.response;
                    $cordovaToast.show($scope.message, "long", "center");

                }).error(function(data, status, header, config){
                  //alert(config.timeout);
                    $scope.message = "Please try again later";
                    $ionicLoading.hide();
                    $cordovaToast.show($scope.message, "long", "center");

                });

    }
    this.unsubscribe = function ($scope) {
         var Apikey = $rootScope.apiKeyOfUser;
          var cpnId=$scope.store_id;
              
                $http({
                method: 'POST',
                url: 'http://lion.27coupons.com/v1.0/post/stores/un-subscribe/',
                data: $.param({ 
                        key:Apikey,
                        id:cpnId
                    }),
                timeout : $rootScope.timeout,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(data){
                    $scope.message=data.response;    
                    if(data.status == 200) {                   
                    $scope.single_stores.store.alert = 0;
                    }            
                  
                    $cordovaToast.show($scope.message, "long", "center");


                        }).error(function(data, status, header, config){
                  //alert(config.timeout);
                            $scope.message = "Please try again later";
                    $ionicLoading.hide();
                            $cordovaToast.show($scope.message, "long", "center");

                });

    }
}])
.service("favorite_del", ["$http","$rootScope","$ionicModal", function ($http,$rootScope,$ionicModal) {
    this.delete = function ($scope,id) {
        $http.get($rootScope.apiUrlHomeGet+"coupons/save-coupon/?key="+$rootScope.apiKeyOfUser,{params: {id: id}},{timeout : $rootScope.timeout})
                .success(function (result) {
                }).error(function(data, status, header, config){
                  //alert(config.timeout);
                  $scope.message="Please try again later";

                    $ionicModal.fromTemplateUrl('templates/save.html', {
                      scope: $scope
                    }).then(function(modal) {
                      $scope.modal = modal;
                      $scope.modal.show();
                    });

                    // Triggered in the login modal to close it
                    $scope.closeLogin = function() {
                      $scope.modal.hide();
                      $('.outer-div').removeClass('blur-in');   
                    };
                });

    }
}])
.service("referService", ["$http","$rootScope","$ionicModal", function ($http,$rootScope,$ionicModal) {
    this.earn = function ($scope) {
        $http.get($rootScope.apiUrlHomeGet+"misc/refer-friend/?key="+$rootScope.apiKeyOfUser,{timeout : $rootScope.timeout})
                .success(function (result) {
                      $scope.headerText= result.response.data.top_html;
                      $scope.linkToBeSharedViaEmail= result.response.data.referral_url;
                      $scope.linkToBeShared= result.response.data.referral_url;
                      $scope.messageText= result.response.data.email_body;
                      $scope.messageSubject= result.response.data.email_subject;
                }).error(function(data, status, header, config){
                  //alert(config.timeout);
                  $scope.message="Please try again later";

                    $ionicModal.fromTemplateUrl('templates/save.html', {
                      scope: $scope
                    }).then(function(modal) {
                      $scope.modal = modal;
                      $scope.modal.show();
                    });

                    // Triggered in the login modal to close it
                    $scope.closeLogin = function() {
                      $scope.modal.hide();
                      $('.outer-div').removeClass('blur-in');   
                    };
                });

    }
}])
.service("locationService", ["$http", "$window", "$cordovaGeolocation", "$q", "$location", "$rootScope", function ($http, $window, $cordovaGeolocation, $q, $location, $rootScope) {

    var locationInfo = {};
    this.sendLocation = function () {
        try{
            var apikey = $rootScope.apiKeyOfUser;
            //alert(apikey);
            var deferred = $q.defer();
            $cordovaGeolocation.getCurrentPosition()
                                .then(function (position) {
                                    var lat = position.coords.latitude;
                                    var long = position.coords.longitude;
                                    //alert("lat=" + lat + "  Long=" + long);
                                    // console.log("http://lion.27coupons.com/v1.0/get/misc/data-logger/?key=" + apikey + "&long=" + long + "&lat=" + lat);
                                    $http.get("http://lion.27coupons.com/v1.0/get/misc/data-logger/?key=" + apikey + "&long=" + long + "&lat=" + lat)
                                        .then(function (response) {
                                            //console.log(response);
                                            locationInfo = {
                                                long: long,
                                                lat: lat,
                                                isSent: 1
                                            }
                                            $window.localStorage["location"] = JSON.stringify(locationInfo);
                                            //alert(response.status);
                                            //$location.path("/app/home");
                                            deferred.resolve(response);
                                        })
                                }, function (err) {

                                });
        } catch (e) {
            delete $window.localStorage["location"];
        }

    }
}])
.service("profileService", ["$http", "$rootScope", "$ionicModal", function ($http, $rootScope, $ionicModal) {
    this.getProfile = function ($scope) {
        $http.get($rootScope.apiUrlHomeGet + "misc/user-profile/?key=" + $rootScope.apiKeyOfUser, { timeout: $rootScope.timeout })
                .success(function (result) {
                    console.log(result.response.data);
                    $scope.userDetails = result.response.data;

                }).error(function (data, status, header, config) {
                    //alert(config.timeout);
                    $scope.message = "Please try again later";

                    $ionicModal.fromTemplateUrl('templates/save.html', {
                        scope: $scope
                    }).then(function (modal) {
                        $scope.modal = modal;
                        $scope.modal.show();
                    });

                    // Triggered in the login modal to close it
                    $scope.closeLogin = function () {
                        $scope.modal.hide();
                        $('.outer-div').removeClass('blur-in');
                    };
                });

    }
}]);
